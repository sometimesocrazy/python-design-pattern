import random
import string

from Controler.ComputerAbstractFactory import ComputerAbstractFactory
from Entities.Computer import Computer
from Entities.Server import Server


class ServerFactory(ComputerAbstractFactory):
    def __init__(self, ram, cpu, hdd):
        self.ram = ram
        self.cpu = cpu
        self.hdd = hdd
        self.ssh_key = self.generate_ssh_key()

    def user_notify(self):
        email = 'admin@admin'  # gui.get_email()
        print('Genrated ssh key : {}\nSend to Email : {}'.format(self.ssh_key, email))

    def generate_ssh_key(self) -> str:
        ssh_key = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
        return ssh_key

    def create_computer(self) -> Computer:
        return Server(self.ram, self.hdd, self.cpu, self.ssh_key)
