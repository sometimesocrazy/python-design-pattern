from Entities.Computer import Computer


class ComputerFactory:

    @staticmethod
    def get_computer(factory) -> Computer:
        return factory.process_request()
