import abc

from Entities.Computer import Computer


class ComputerAbstractFactory():

    def process_request(self):
        computer = self.create_computer()
        self.user_notify()
        return computer

    @abc.abstractmethod
    def create_computer(self) -> Computer:
        raise NotImplementedError("Should have implemented this")

    @abc.abstractmethod
    def user_notify(self):
        raise NotImplementedError("Should have implemented this")
