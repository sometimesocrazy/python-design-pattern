from Controler.ComputerAbstractFactory import ComputerAbstractFactory
from Entities.Computer import Computer
from Entities.PC import PC


class PCFactory(ComputerAbstractFactory):
    def __init__(self, ram, cpu, hdd):
        self.ram = ram
        self.cpu = cpu
        self.hdd = hdd

    def user_notify(self):
        print('Create PC with default password : {}'.format('fsoft@12345'))

    def create_computer(self) -> Computer:
        return PC(self.ram, self.hdd, self.cpu)
